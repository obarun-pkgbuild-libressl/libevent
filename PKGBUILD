# Maintainer  : Jean-Michel T.Dydak <jean-michel@obarun.org> <jean-michel@syntazia.org>
#--------------
## PkgSource  : https://www.archlinux.org/packages/core/x86_64/libevent/
## Maintainer : Tom Gundersen <teg@jklm.no>
## Contributor: Judd <jvinet@zeroflux.org>
#--------------------------------------------------------------------------------------

pkgname=libevent
pkgver=2.1.8
pkgrel=4
arch=('x86_64')
license=('BSD')
_website="http://libevent.org/"
pkgdesc="An event notification library"
url="https://github.com/libevent/libevent"
source=("$url/releases/download/release-$pkgver-stable/$pkgname-$pkgver-stable.tar.gz"{,.asc}
    'libevent-libressl.patch')

depends=(
    'libressl')

optdepends=(
    'python2: to use event_rpcgen.py')

#--------------------------------------------------------------------------------------
prepare() {
    cd "$pkgname-$pkgver-stable"

    ## libevent with libressl
    patch -Np1 -i ../libevent-libressl.patch
}

build() {
    cd "$pkgname-$pkgver-stable"

    ./autogen.sh
    ./configure                    \
        --prefix=/usr              \
        --sysconfdir=/etc          \
        --disable-libevent-regress
    make
}

check() {
    cd "$pkgname-$pkgver-stable"

    make -j1 check
}

package() {
    cd "$pkgname-$pkgver-stable"

    make DESTDIR="${pkgdir}" install

    install -Dm 644 LICENSE "${pkgdir}"/usr/share/licenses/$pkgname/LICENSE
}

#--------------------------------------------------------------------------------------
validpgpkeys=('B35BF85BF19489D04E28C33C21194EBB165733EA'
              '9E3AC83A27974B84D1B3401DB86086848EF8686D'
)
sha512sums=('a2fd3dd111e73634e4aeb1b29d06e420b15c024d7b47778883b5f8a4ff320b5057a8164c6d50b53bd196c79d572ce2639fe6265e03a93304b09c22b41e4c2a17'
            'SKIP'
            'c54e9951d6141bcd7d05c42502ec60f79704f4a4056654146b9aef3313f0c530b991fc7649348006ede3cbc9609a743ca31ab9aa5d868a7aae8062b7ee5ac446')
